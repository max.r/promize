package com.rahasak.promize

import scala.concurrent.{ExecutionContext, Future}

object FutureOps {
  def getUrl(id: Long)(implicit ec: ExecutionContext): Future[String] = {
    Future {
      Thread.sleep(100)

      s"https://www.example.org/?id=$id"
    }
  }
}
