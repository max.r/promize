name := "promize"

version := "1.1"

scalaVersion := "2.13.0"

libraryDependencies ++= {

  val akkaVersion = "2.5.23"
  val logbackClassicVersion = "1.2.3"
  val slf4jApiVersion = "1.7.26"
  val sprayJsonVersion = "1.3.5"

  val scalaTestVersion = "3.0.8"

  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "io.spray" %% "spray-json" % sprayJsonVersion,
    "org.slf4j" % "slf4j-api" % slf4jApiVersion,
    "ch.qos.logback" % "logback-classic" % logbackClassicVersion,

    "org.scalatest" %% "scalatest" % scalaTestVersion % "test",
  )
}
