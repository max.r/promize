package com.rahasak.promize

import org.scalatest.AsyncFlatSpec

class FutureOpsSpec extends AsyncFlatSpec {
  behavior of "FutureOps.getUrl"

  it should "eventually return an URL" in {
    val future = FutureOps.getUrl(1)

    future map { url => assert(url == "https://www.example.org/?id=1")}
  }
}
